# Markov's Simple Twitter Bot

Just a fun little script that pulls most recent samples from given Twitter
accounts, generates a simple Markov chain, and spits out corresponding 
messages.

Currently the Twitter API access tokens and the target accounts are hard-coded
in the Python source.

Task List:
* [] Add input file for key & secret
* [] Add input file for Twitter account targets.
