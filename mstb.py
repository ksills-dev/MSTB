#!/bin/python

# Markov's Simple Twitter Bot
# ---------------------------
# Just a fun little script that pulls most recent samples from given Twitter
# accounts, generates a simple Markov chain, and spits out corresponding 
# messages.
#
# TODO: [] Add input file for key & secret
# TODO: [] Add input file for Twitter account targets.

import random
import sys

import tweepy
from tweepy import OAuthHandler

# Settings
verbose = True
seed = None
users = ['voidsexts', 
         'NightVWeather']
sample_size = 100
n_generations = 25

print('Settings')
print('========')
print('Seed: {}'.format(seed))
print('UserID: {}'.format(users))
print('Desired Sample Size: {}'.format(sample_size * len(users)))
print('Number of Generated Phrases: {}'.format(sample_size))
print()

# Authenticate

auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)

api = tweepy.API(auth)

# Get tweets for analysis
tweets = []
for user in users:
  print("Fetching tweets for {}... ".format(user), end='')
  sys.stdout.flush()
  user_count = 0
  for tweet in tweepy.Cursor(api.user_timeline, id=user, include_rts=False).items(sample_size):
    user_count += 1
    # Stop using account-specific stuff.
    # tweets.append(tweet.text.lower().lstrip('sext: '))
  
  print("Done!")
  print("Fetched {:d} /{:d} ({:d}%) requested tweets." \
        .format(user_count, sample_size, int(float(user_count) / sample_size * 100)))
  print()

print("Finished fetching tweets.")
print()

# Find occurance counts
occurences = {'[':{}}

for tweet in tweets:
  #print('*' + tweet)
  words = tweet.replace(',', '') \
               .replace('.', '') \
               .replace(':', '') \
               .replace(';', '') \
               .replace('-', '') \
               .replace('(', '') \
               .replace(')', '') \
               .replace(')', '') \
               .replace('&amp', '&') \
               .split()
  words = ['['] + words + [']']
  for i in range(1, len(words)):
    past_word = words[i-1]
    word = words[i]
    if past_word not in occurences:
      occurences[past_word] = {}
    
    if word not in occurences[past_word]:
      occurences[past_word][word] = 1
    else:
      occurences[past_word][word] += 1
        
# Build Markov Chain
mchain = {}
for base_word, children in occurences.items():
  mchain[base_word] = ([], [])
  for child_word, count in children.items():
    mchain[base_word][0].append(child_word)
    mchain[base_word][1].append(count)
     
# Generate phrases
random.seed(seed)
def generate_phrase(mchain):
  generated = ''
  current_word = '['
  while True:
    choices = mchain[current_word][0]
    weights = mchain[current_word][1]
    try:
      current_word = random.choices(choices, weights).pop()
    except IndexError:
      print("{0}\n\n{1}\n\n{2}\n".format(current_word, choices, weights))
      break
    if current_word == ']':
      break
    generated = generated + ' ' + current_word
  return generated

for i in range(0, n_generations):
  print("{:d}) {:s}".format(i+1, generate_phrase(mchain)))
